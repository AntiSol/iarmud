class Interpreter
	@@direction_shortcuts = { 
		"n" => "north", "nw" => "northwest", "w" => "west", "sw" => "southwest",
		"s" => "south", "se" => "southeast", "e" => "east", "ne" => "northeast",
		"u" => "up", "d" => "down" 
	}
	
	#this is a map of 'verb' => 'function_name'
	#the function may or may not exist on subclasses of Mappable
	#	if it doesn't, the item is not <verb>-able
	#	note aliasing - multiple things point to v_attack
	@@verbs = {
		'take' => 'v_pickup',
		'pickup' => 'v_pickup',
		'examine' => 'v_examine',
		'drop' => 'v_drop',
		'throw' => 'v_throw',
		'attack' => 'v_attack',
		'kill' => 'v_attack',
		'maim' => 'v_attack',
		'decapitate' => 'v_attack',
		'behead' => 'v_attack',
		'immolate' => 'v_attack',
		'disembowel' => 'v_attack',
		'destroy' => 'v_destroy',
		'eat' => 'v_eat',
		'consume' => 'v_eat',
	}
	
	#shortcuts to commands 
	#	these are effectively overrides to the builtin shortcut system
	@@cmd_shortcuts = {
		'i' => 'inventory',
		'm' => 'map',
		'l' => 'look',
		'f' => 'forge',
		't' => 'tunnel',
	}
  
	def initialize 
		#it's important that word-style join strings contain spaces, because
		#	we wouldn't want to e.g split the word 'thousand'
		@join_strs = [" then ",";",","," also "," and "]
		message "Interpreter init"
		
		#create command methods for the available directions...
		Coordinate(0,0,0).methods.find_all{ |m| m =~ /dir_\w+/ }.each do |m|
			m = m.to_s.sub('dir_','')
			nm = "cmd_#{m}".to_sym
			dm = "desc_#{m}".to_sym
			self.define_singleton_method nm do
				@@args = [m]
				cmd_go 
			end
			self.define_singleton_method dm do
				"<hidden>"
			end
		end
		
		#special command shortcuts...
		#we create special shortcut functions (cmd_i -> cmd_inventory)
		#to override the builtin shortcut system.
		@@cmd_shortcuts.each_pair do |shortcut, cmd|
			nm = "cmd_#{shortcut}"
			dm = "desc_#{shortcut}"
			god = "god_#{cmd}"
			self.define_singleton_method nm do
				#ret.push('(#{cmd})')
				#ret.push(self.send("cmd_#{cmd})"))
				self.send("cmd_#{cmd}")
			end
			#shortcut overrider don't show up in help
			self.define_singleton_method dm do
				"<hidden>"
			end
			
			if self.respond_to?(god) && self.send(god)
				self.define_singleton_method "god_#{shortcut}" do
					true
				end
			end
		end
		
		#create shortcuts for directions...
		@@direction_shortcuts.each do |shortcut, direction|
			nm = "cmd_#{shortcut}"
			dn = "desc_#{shortcut}"
			self.define_singleton_method nm do
				@@args = [direction]
				cmd_go
			end 
			self.define_singleton_method dn do
				"<hidden>"
			end
		end
		
		#create functions for verbs...
		@@verbs.each_pair do |k,v|
			nm = "cmd_#{k}"
			dm = "desc_#{k}"
			self.define_singleton_method nm do
				#@@args should refer to a mappable in the players location.
				# or in teh player's inventory
				# TODO: we should check that there's only 1 match and ask 
				#	the player to be more specific if >1
				
				return PlayerNotificationEvent.new("What do you want to #{k}?",@@player) if @@args.length == 0
				
				itm = $game.world.location(@@player.coord).contents.find { |i| i.name.downcase.include?(@@args[0]) }
				if !itm #look in player's inventory...
					itm = @@player.inventory.find { |i| i.name.downcase.include?(@@args[0]) }
					return PlayerNotificationEvent.new("You don't see #{@@args[0]} here!",@@player) if !itm
				end 
				if itm.respond_to?(v)
					return itm.send(v,@@player) #we should probably pass @@args aswell,
												# to allow 'attack grue viciously with giant spikey thing'
				else
					#item exists but isn't verb-able...
					return "You can't #{k} #{itm.name}!"
				end
			end
			#hide verb functions from help, it has it's own verb section.
			self.define_singleton_method dm do
				"<hidden>"
			end
		end 
	end
	
	def to_c(method_name)
		#return the command which maps to a function name: 'cmd_help' returns 'help'
		ret = method_name
		prefixes = ["cmd_","desc_","help_", "god_"]
		prefixes.each { |p| 
			ret.sub!(p,"")
		}
		ret
	end

	#teh workworse: interprets a command for a player.
	# cmd is a string which is raw player input.
	# it might include multiple commands chained together.
	# interpret interprets the first of these, and appends the rest to the
	# player's command queue.
	def interpret(player, cmd)
		return false if !cmd
		@@player = player
		#message "Interpreting '#{cmd}'"
		ret = []
		tmp = []
		cmds = []
		#handle multiple split strings...
		@join_strs.each do |str|
			if tmp.length == 0
				tmp = cmd.split(str)
			else
				tmp2 = []
				tmp.each do |c|
					tmp2 += c.split(str)
				end
				tmp = tmp2
			end
		end
		cmds = tmp
		message "--- Commands: #{cmds}"
		c = cmds.shift
		player.command_queue += cmds if cmds.length > 0 #queue any subsequent commands
		return false if !c
		#message "processing: #{c}"
		#message "Command queue:"
		#puts player.command_queue
		#cmds.each do |c|
			c.strip!
			#message "Parsing command: '#{c}'"
			words = c.split
			fn = "cmd_#{@@command = words[0]}"
			god = "god_#{@@command}"
			words.shift
			q = false
			tmp = ""
			wd = []
			#handle quoted params...
			words.each { |w|
				if w[0] == '"'
					tmp = w.slice(1,w.length - 1)
					q = true
					if w[w.length-1] == '"'
					 tmp = tmp.slice(0,tmp.length-1)
					 wd.push(tmp)
					end
					next
				end
				if w[w.length-1] == '"'
					q = false
					tmp += " " + w.slice(0,w.length - 1)
					wd.push(tmp)
					next
				end
				tmp += " " + w if q
				wd.push(w) if !q  
			}
			@@args = wd
			
			ok = false
			message "processing cmd:#{@@command}, args:#{@@args} for #{@@player}"

			if self.respond_to?(fn)
				ok = true
				if self.respond_to?(god) && self.send(god)
					#god required, check and fail if we're not a god...
					if !@@player.is_god
						ret.push(PlayerNotificationEvent.new("Only gods can #{@@command}.",@@player))
					else
						ret.push(self.send(fn))
					end
				else
					ret.push(self.send(fn))
				end
			else
				if @@command and @@command.length == 1	#allow 1-char abbreviations for commands
					m = self.methods.find_all { |f| f.slice(0,5) == fn}
					if m.length == 1		#but only if there's 1 command starting with that char
						m = m[0]
						r = to_c(m.to_s)
						r += " #{@@args.join(' ')}" if @@args.length > 0
						ret.push(PlayerNotificationEvent.new("(#{r})",@@player))
						ret.push(self.send(m))
						ok = true
					end
				end
				ret.push(PlayerNotificationEvent.new("I don't know how to '#{c}'!\n",@@player)) if !ok
			end
		#end
		
		#add events generated by commands to the event queue...
		$game.queue_event(ret)
	end

	# ---command functions---
	#
	# - method names MUST start with cmd_ (i.e: 'help' maps to cmd_help)
	#		methods should not accept any parameters, use the @@args array for that
	#	**Important: Command Functions should return an Event of some kind, 
	#		probably some kind of PlayerNotificationEvent, e.g:
	#		PlayerNotificationEvent.new("text",@@player)
	#		events returned will be queued automatically.
	#
	#		for more complicated things, you probably want to create your own
	#		Event descendant
	#
	# - commands should also define a method with a desc_ prefix
	#		this method should return a 1-line description of the command
	#		to be used in the 'commands' section of 'help' with no arguments.
	#		if you return "<hidden>" as your description, your command will not 
	#		be listed by the 'help' command.
	#
	# - commands may define a method with a help_ prefix. This method should return
	#		long text describing the command in full. this is called when the 
	#		player types 'help <command>'
	#
	# - commands may define a method with a god_ prefix. This method should
	#		return a boolean indicating whether the command requires god
	#		privileges to run. default is false.

	def cmd_quit
		$playing = false
		PlayerNotificationEvent.new("\nYou flee like a coward. Your foes laugh heartily.\n\n",@@player)
	end
	
	def desc_save
		"Saves World State"
	end
	def cmd_save
		$game.save("iarmud.map.gz")
	end
	
	def cmd_load
		$game.load('iarmud.map.gz')
	end
	
	#create a location or item.
	# you say 'forge <location_type> <direction>'
	#	or 'forge <mappable_type> <name>'
	# mappables are forged in the current location
	def cmd_forge
		if @@args.length < 2 
			return help_forge
		else
			#The specified class needs to be a descendant of Mappable, but not
			#	a Creature 
			begin
				k = Kernel.const_get(@@args[0])
			rescue
				return PlayerNotificationEvent.new("You can't forge a '#{@@args[0]}'!",@@player)
			end
			
			if k < Location
				#a location class has been specified, @@args[1] should be a 
				#	direction.
			else
				#the class should be a non-Creature Mappable. 
				#	@@args[1] should be a name for the created Mappable
				return PlayerNotificationEvent.new("You can't forge a '#{@@args[0]}'!",@@player) if !(k < Mappable) || (k < Creature) || (k == Creature)
				n = @@args[1]
				i = k.new(n,"a new #{n}")
				$game.world.add_mappable(@@player.coord,i)
				ret = "You forge a #{k} named '#{n}'"			
			end
		end
		#ret
		return PlayerNotificationEvent.new(ret,@@player)
	end
	def desc_forge
		"Create Prefab Items or locations"
	end
	def help_forge
		ret = "You can forge the following prefab locations with\n  'forge <location> <direction>':\n"
		Location.subclasses.each { |k| ret += "\t- #{k}\n" }
		ret += "\nYou can forge the following items in the current\n  location with 'forge <item> <name>':\n"
		Mappable.subclasses.each { |k| ret += "\t- #{k}\n" if !(k < Creature) && k != Creature } #you can't forge a creature - these are spawned.
		ret
	end
	def god_forge
		true
	end

	def desc_fuck
		"<hidden>"
	end
	def cmd_fuck
		return "Who?" if @@args.length == 0 
		PlayerNotificationEvent.new("#{@@args.join(' ')} says: 'oooooh, baby!'",@@player)
	end
	
	def desc_map
		"Displays a map"
	end
	def help_map
		
	end
	def cmd_map
		ret = "MAP SURROUNDING #{@@player.coord}:\n"
		ret += $game.world.render_ascii_size(@@player.coord,80,20,@@player)
		PlayerNotificationEvent.new(ret,@@player)
	end
	
	def cmd_god
		PlayerNotificationEvent.new(@@player.god(@@args[0]),@@player)
	end
	def desc_god
		"Enable god commands"
	end
	def help_god
		"  god <key>\t - Authenticate using the specified god token. the god token is output to the server's console at game startup.\n"
	end
	
	def desc_who
		"Shows a list of players"
	end
	def cmd_who
		ret = "Players in the game:\n"
		$game.players.each do |p|
			if p == @@player
				ret += "\t- #{p}*\n"
			else
				ret += "\t- #{p}\n"
			end
		end
		ret += "\nTOTAL: #{$game.players.count} player(s)."
		PlayerNotificationEvent.new(ret,@@player)	
	end
	
	def cmd_inventory
		ret = "You are carrying:\n"
		if @@player.inventory.length == 0
			ret += " - a whole lotta nothing."
		else
			# Consolidated list
			# Possibly an easier way to do this.
			counts = {}
			@@player.inventory.each { |item| counts[item.name] ||= []; counts[item.name].push item }
			counts.each_key do |key|
				tmp = " - #{key}"
				tmp += " (#{counts[key].length})" if counts[key].length > 1
				ret += tmp + "\n"
			end
			ret += "(#{@@player.inventory.length} items)"
		end
		PlayerNotificationEvent.new(ret,@@player)
	end
	def desc_inventory
		"mysterious"
	end
	

	def desc_version
		"Displays the server version"
	end
	def cmd_version
		PlayerNotificationEvent.new($game.title,@@player)
	end	
	
	def desc_go
		"Moves your player"
	end
	def help_go
		"go <dir>\t- move in the specified direction.\n\n" +
		"This command is completely redundant - there are \n" +
		"  many other ways of calling it, all the standard\n" +
		"  text shortcuts apply: 'go north' = 'north' = 'n', etc."
	end
	def cmd_go
		return PlayerNotificationEvent.new("you need to speficy a direction!",@@player) if @@args.length == 0
		#transform 'n' to 'north'...
		@@args[0] = @@direction_shortcuts[@@args[0]] if @@direction_shortcuts.has_key?(@@args[0])
		
		#@@player.move(@@args[0])
		#this is how it should be done: queue a PlayerMovementEvent 
		PlayerMovementEvent.new(@@player,@@args[0])
	end
	
	def cmd_tunnel
		return PlayerNotificationEvent.new("you need to specify a direction!",@@player) if @@args.length == 0
		#transform shortcuts...
		@@args[0] = @@direction_shortcuts[@@args[0]] if @@direction_shortcuts.has_key?(@@args[0])
		#@@player.tunnel(@@args[0])
		PlayerTunnelEvent.new(@@player,@@args[0])
	end
	
	def cmd_look
		return PlayerLookEvent.new(@@player)
	end
	
	def cmd_title
		return PlayerNotificationEvent.new("Set your title to what?",@@player) if @@args.length == 0
		@@player.title = @@args.join(" ")
		cmd_whoami
	end
	def desc_title
		"Sets your title"
	end
	
	def cmd_pick
		message "picking nose"
		return NosePickEvent.new(@@player)
	end
	
	def cmd_name
		return PlayerNotificationEvent.new("Set your name to what?",@@player) if @@args.length == 0
		#TODO: check that no player with this name already exists - 
		#	names need to be unique
		@@player.name = @@args.join(" ")
		cmd_whoami
	end
	def desc_name
		"Changes your name"
	end
	
	def cmd_whoami
		PlayerNotificationEvent.new("You are '#{@@player}'",@@player)
	end
	
	#help command...
	def desc_help 
		"This Message" 
	end
	def help_help
		"With no parameters, 'help' shows a list of commands.\nIf given a command, 'help' shows full help for that command."
	end
	def cmd_help
		
		#help for a single command...
		if @@args.length > 0
			b = ""
			if self.respond_to?("cmd_#{@@args[0]}")
				if self.respond_to?("help_#{@@args[0]}")
					a = "'#{@@args[0]}' - #{self.send("desc_#{@@args[0]}")}\n\n#{(b = self.send("help_#{@@args[0]}"))}\n\n"
				end
			end
				
			return a if b != ""
			return PlayerNotificationEvent.new("No Help Available.",@@player)
		end
		
		#no arguments, General help...
		
		msg = "Welcome to " + $game.title + ".\n"

		msg += "\n---MOVEMENT (DIRECTIONS)---\nYou can bypass the 'go' command by just typing a direction.\n" +
			"IARMUD knows about the following directions:\n  "
		a=0
		@@direction_shortcuts.each_key { |k| 
			a+= 1
			msg += "#{@@direction_shortcuts[k]} (#{k}), "
			msg += "\n  " if a % 4 == 0
		}
		msg = msg.slice(0,msg.length - 2)		

		msg += "\n\n---ACTIONS (VERBS)---\nYou can perform actions on objects in the game " + 
			"by using verbs.\nVerb commands take the form <verb> <object>, e.g: " +
			"'kill grue'.\nNot every verb applies to every object - you can't " +
			"kill a lantern!\n\nIARMUD knows the following verbs (sorted alphabetically):\n\n  "
		a=0
		@@verbs.keys.sort.each { |v|
			a+= 1
			msg += "#{v}, "
			msg += "\n  " if a % 6 == 0			
		}
		msg = msg.slice(0,msg.length - 2)

		msg += "\n\n---AVAILABLE COMMANDS---\n (type 'help <command>' for more detailed help)\n\n"
		self.methods.find_all{ |m| m =~ /cmd_\w+/ }.sort.each do |m|
			m = m.to_s
			c = to_c(m)
			dm = "desc_#{c}"
			gm = "god_#{c}"
			#hide god commands if the player isn't god
			if self.respond_to?(gm) && self.send(gm)
				next if !@@player.is_god
				m = "*" + m	#indicate god commands
			end
			if (!self.respond_to?(dm))
				desc = '(No Description)'
			else 
				desc = self.send(dm)
			end
			if s = @@cmd_shortcuts.find { |s,cmd| cmd == c }
				m += " (#{s[0]})"
			end
			msg += "#{m.ljust(20)} - #{desc}\n" if desc.downcase != '<hidden>'
		end
		if @@player.is_god
			msg += "\n* - God Commands\n"
		end
		
		msg += "\n---SHORTCUTS---\nIf there's only one command which starts with\n" +
			"  a given letter, you can shortcut to that command with that letter.\n" +
			"  (i.e: you can type 't' for 'tunnel'.). In addition, certain oldskool\n" +
			"  conventions apply ('I', 'L', etc)."

		msg += "\n\n---CHAINING COMMANDS---\nYou can chain multiple commands together with: "
		a=0
		ex = ""
		excmds = ["go north","attack rabbit","Run away!","hide","hide some more","Talk to brother maynard","Take Holy Hand Grenade","Count to 3...uh...five"]
		@join_strs.sort.each { |v|
			a+= 1
			msg += "'#{v}'"
			if a == (@join_strs.length() -1)
				msg += " or " 
			else 
				msg += ", " if a < @join_strs.length()
			end
			ex += excmds[a-1] + v + (v =~ /[,;]/ ? " " : "") 
			ex += excmds[a] if a == (@join_strs.length())  
			msg += "\n  " if (a % 6 == 0) or a == 3
		}	
		msg += ".\nExamples:\n  '" + ex + "'\n  's;s;s;s;s;s;s;s;s;s;s;s;s;s;s;s;s;s;s;s;s;s'\n\nNote that chained actions are queued up, " +
			"and will take time to happen.\nFurther commands won't be executed until the " +
			"end of the queue.\n"
	
		return PlayerNotificationEvent.new(msg,@@player)
	end
end
