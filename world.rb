#put a call to this inside a class definition to make the class
#	aware of its descendants. you can then simply access Class.subclasses
def knows_descendants!
	class << self
		def inherited sub
			@subclasses ||= []
			@subclasses << sub
		end

		attr_reader :subclasses
	end
end

#this holds our map. its core is a hash of hashes of hashes of Locations. 
#	it expands as Locations are added
class World
	attr_reader :xmin, :xmax, :ymin, :ymax, :zmin, :zmax, :map
	def initialize(filename = nil)
		@map = { 0 => false }
		@xmin = @xmax = @ymin = @ymax = @zmin = @zmax = 0
		add_location(Coordinate(0,0,0),SpawningPool.new)
		return self
	end
	
	def location(coord)
		return nil if !@map[coord.z] || !@map[coord.z][coord.x]
		@map[coord.z][coord.x][coord.y]
	end
	
	def all_locations(including_rock = false)
		ret = []
		@map.each_key do |z|
			@map[z].each_key do |y|
				@map[z][y].each_key do |x|
					ret.push(@map[z][y][x]) if (including_rock) || (!@map[z][y][x].is_a? Rock)
				end
			end
		end
		ret
	end
	
	def find_locations(title = nil, classname = nil)
		ret = []
		all_locations.each do |loc|
			ret.push(loc) if (loc.title == title) || (loc.is_a? classname)
		end
		ret
	end
	
	def all_mappables
		ret = []
		all_locations.each do |loc|
			loc.contents.each { |m| ret.push(m) if m.is_a? Mappable }
		end
		ret
	end
	
	def remove_player(name)
		all_locations.each do |loc|
			loc.contents.each { |i| 
				loc.contents.delete(i) if i.name == name && i.is_a?(Player) 
			}
		end
	end
	
	#this shouldn't be used often - $game.player holds the same info but is much less expensive
	#	this is used when loading a map...
	def find_players
		all_mappables.find_all{ |m| m.is_a? Player }
	end

	#add a location to the map.
	#	@map[z][y][x] will be created if necessary (the map expands). 
	#	You can't overwrite an existing location, but you can overwrite Rock.
	#	x, y, and z on the Location will be set.
	#	rock will be generated to surround the new location if necessary
	#	(all non-rock locations need to be next to either rock or another location)
	def add_location(coord,location)
		if (a = location(coord)) && (! a.is_a? Rock)
			return FailureEvent.new("There's already something at (#{coord.x},#{coord.y},#{coord.z})!")
		end
		return FailureEvent.new("Invalid location!") if !location.is_a? Location
		@xmax = coord.x if coord.x > @xmax
		@ymax = coord.y if coord.y > @ymax
		@zmax = coord.z if coord.z > @zmax
		@xmin = coord.x if coord.x < @xmin
		@ymin = coord.y if coord.y < @ymin
		@zmin = coord.z if coord.z < @zmin
		location.coord = coord
		if !@map[coord.z]
			@map[coord.z] = { coord.x => { coord.y => location } }
		elsif !@map[coord.z][coord.x]
			@map[coord.z][coord.x] = { coord.y => location }
		else
			@map[coord.z][coord.x][coord.y] = location
		end
		
		
		#yay for lack of infinite recursion! don't place more rock around rock...
		return location if location.is_a? Rock
		
		#generate rock in empty space surrounding the new location...
		coord.adjacents.each do |dir,coord|
			add_location(coord,Rock.new) if !location(coord)
		end
		
		return location
	end
	
	def destroy_location(coord)
	  #(never destroy 0,0,0)
	  return false if !location(coord)
		@map[coord.z][coord.x].delete(coord.y) if coord.y != 0
		@map[coord.z].delete(coord.x) if @map[coord.z][coord.x].length == 0 && coord.x != 0
		@map.delete(coord.z) if @map[coord.z].length == 0 && coord.z != 0
		#TODO: adjust x/y/zmin/max
	end
	
	def add_mappable(coord,mappable)
		return FailureEvent.new("Nothing at #{coord}!") if (!loc = location(coord)) || loc.is_a?(Rock)
		return FailureEvent.new("Invalid mappable!") if !mappable.is_a? Mappable
		mappable.coord = coord
		loc.contents.push(mappable)
	end

	def move_mappable(mappable,to)
		#to is a coordinate
		from = mappable.coord
		add_mappable(to,mappable)
		location(from).contents.delete(mappable)
	end
	
	def remove_mappable(mappable)
		return if !mappable.coord.in_world
		location(mappable.coord).contents.delete(mappable)
		mappable.coord = Coordinate(nil,nil,nil)
	end

	def adjacent_locations(coord)
		ret = Hash.new
		coord.adjacents.each do |dir,c|
			if a = location(c)
				ret[dir] = a
			end
		end
		ret
	end
	
	def render_ascii(coord,radius,player=nil,mode=:ascii)
		ret = ""
		(coord.y-radius).upto(coord.y + radius).each do |y2|
			(coord.x-radius).upto(coord.x + radius).each do |x2|
				l = location(Coordinate(x2,y2,coord.z))
				case mode
					
				#TODO: insert more mapping modes here.
				#	'l' will either be a world location, or nill
				#	you should add 1 char to 'ret' in your case
				
				
				#class mode - render map by location class
				when :class
					
					if l 
						ret += l.class.to_s[0]
					else 
						ret += " "
					end
					
				else	#default mode - ascii
					if l
						p = l.players
						if player && p.find { |a| a == player }
							ret += "@"
						else
							case p.length
								when 0
									ret += l.map_char
								when 1	#only 1 player? first letter of name...
									ret += p[0].name[0]
								when 2..9
									ret += p.length.to_s
								else
									ret += ">"
							end
						end
					else
						ret += "#"	#empty space...
					end
				end
			end
			ret += "\n"
		end
		ret
	end
	
	def render_ascii_size(coord,width,height,player=nil,mode=:ascii)
		map = render_ascii(coord,width > height ? width/2 : height/2,player,mode)
		ls = map.split("\n")
		ret = []
		if width > height
			#trim lines from top and bottom of the map...
			dif = width - height
			(dif/2).to_i.upto(width - (dif/2).to_i).each do |ln|
				ret.push(ls[ln])
			end
		else
			#trim columns...
			dif = height - width
			s = (dif / 2)
			e = (height - (dif / 2).to_i) - s
			ls.each { |ln| ret.push(ln.slice(s,e)) }
		end
		ret = ret.join("\n")
	end
	
end


#convenience shortcut: Coordinate(x,y,z) instead of Coordinate.new(x,y,z)
def Coordinate(x,y,z)
	Coordinate.new(x,y,z)
end
class Coordinate
	attr_accessor :x, :y, :z
	def initialize(x,y,z)
		@x,@y,@z = x,y,z
	end
	
	def in_world
		return (x != nil) && (y != nil) && (z != nil)
	end
	
	#direction functions. return a coordinate relative to the current one.
	def dir_north
		Coordinate(@x,@y-1,@z)
	end
	
	def dir_south
		Coordinate(@x,@y+1,@z)
	end
	
	def dir_west
		Coordinate(@x-1,@y,@z)
	end
	
	def dir_east
		Coordinate(@x+1,@y,@z)
	end
	
	def dir_up
		Coordinate(@x,@y,@z+1)
	end
	
	def dir_down
		Coordinate(@x,@y,@z-1)
	end
	
	def dir_northeast
		dir_north.dir_east
	end
	
	def dir_northwest
		dir_north.dir_west
	end
	
	def dir_southeast
		dir_south.dir_east
	end
	
	def dir_southwest
		dir_south.dir_west
	end
	
	#we don't want 'northeast-down', do we???
	
	
	#Arrays of adjacent coordinates.
	#	note that for the coordinate object, there will always be a full set of adjacents
	#	because the map expands arbitrarily. For a Location object, adjacents may or may not exist (and/or be Rock)
	
	#convention is for coordinates to be returned in clockwise order
	
	def adjacents
		{ "north" => dir_north, 
			"northeast" => dir_northeast, 
			"east" => dir_east, 
			"southeast" => dir_southeast, 
			"south" => dir_south, 
			"southwest" => dir_southwest, 
			"west" => dir_west, 
			"northwest" => dir_northwest, 
			"up" => dir_up, 
			"down" => dir_down
		}
	end
	
	def diagnonals
		[ dir_northwest, dir_northeast, dir_southwest, dir_southeast ]
	end
	
	def adjacents_2D
		[ dir_north, dir_northeast, dir_east, dir_southeast, dir_south, dir_southwest, dir_west, dir_northwest, dir_up, dir_down]
	end
	
	def adjacents_vert
		[ dir_up, dir_down ]
	end
	
	def adjacents_nsew(in_that_order = false)
		return [ dir_north, dir_south, dir_east, dir_west] if in_that_order
		[ dir_north, dir_east, dir_south, dir_west ]
	end
	
	def to_s
		return "(an empty coordinate)" if !in_world
		"(#{@x},#{@y},#{@z})"
	end
end

#this is a location on our map - one unit of player movement. 
#	This (and descendants) are the only objects that can be added to the World.
#It has things like: coordinates, a title, description, 
# 	functions to provide adjacent locations, and an array of Mappables which 
#	represent the contents of the location.
# this should probably have 'owner', 'fortified', so that players can own 
#	areas of the map and lock them from modification by other players.
class Location
	attr_accessor :coord, :map_char, :contents
	attr_reader :title, :description
	
	knows_descendants!
	
	def initialize(title, description = nil, map_char = " ")
		@title = title
		@description = description
		@map_char = map_char
		@coord = Coordinate(nil,nil,nil)
		@contents = []
	end
		
	#shortcuts for coordinate
	def x
		@coord.x
	end
	def x=(x)
		@coord.x = x
	end
	def y
		@coord.y
	end
	def y=(y)
		@coord.y = y
	end
	def z
		@coord.z
	end
	def z=(z)
		@coord.z = z
	end
	
	def in_world
		return (x != nil) && (y != nil) && (z != nil)
	end
	
	def add_to_world(world,coord)
		world.add_location(coord,self)
	end
	
	def players
		@contents.find_all { |i| i.is_a? Player }
	end
	
	def to_s
		"#{coord}: #{title}"
	end
	
	def exits
		ret = ""
		$game.world.adjacent_locations(@coord).each { |dir,loc|
			ret += "#{dir.capitalize} there is #{loc.title}.\n" if !loc.is_a? Rock
		}
		ret
	end
	
	def contents_as_s(exclude = nil)
		ret = ""
		@contents.each { |i| 
			next if (i.name == exclude) || i.class == exclude
			if i.is_a? Player
				ret += "#{i} is here."
				ret += " #{i} is #{i.action}." if i.action
				ret += "\n"
			else
				ret += "There is #{i} here.\n"
			end
		}
		ret
	end
end

#descendants of Location...
#TODO: a 'roomtypes.rb' file with a bunch of these...?
#you can put prefab room types here, i.e:
class Castle < Location
  def initialize
    super('A Castle',"An Enormous Castle. With Towers and whatnot.","^")
  end
end

#some special locations:

# - our base 'impassable' class
class Rock < Location
	def initialize
		title = ["Impassable","Solid", "Much", "A Whole Lotta"].shuffle[0] + 
			" " + ["Dirt", "Rock","Granite","Limestone"].shuffle[0]
		super(title,nil,"*")
	end
end

# - what is created with the 'tunnel' command
class Tunnel < Location
	def initialize
		super("A Tunnel","A Fresh, bland tunnel."," ")
	end
end

# - players spawn here. There's one at (0,0,0).
class SpawningPool < Location
  def initialize
    super("A Spawning Pool","Where Avatars are born!","S")
  end
end

#include this module in any Mappable class that can be picked up by a player
module Takeable
	def v_pickup(player)
		@held_by = player
		player.add_to_inventory(self)
	end	
	
	def v_drop(player)
		return "You're not carrying #{name}!" if player != @held_by
		player.remove_from_inventory(self)
		$game.world.add_mappable(coord,self)
		@held_by = nil
		PlayerNotificationEvent.new("You toss #{name} aside carelessly.",player)
	end
end

#this is a base class for any Item which a Location can contain:
#	any object in our Game world, from players to AIs to swords and gold.
# you differentiate items in the game world based on their class.
# it doesn't have much in the way of attrubutes: a reference to its location, 
#	a name and description
class Mappable
	attr_accessor :name, :description, :coord, :indefinite
	knows_descendants!
	
	include Takeable
	
	def initialize(name,description=nil)
		@name, @description = name, description
		@coord = Coordinate(nil,nil,nil)
		@held_by = nil
		@indefinite = "an"
	end
		
	def v_examine(player)
		return PlayerNotificationEvent.new("You examine #{name}:\n\n#{description}",player)
		PlayerNotificationEvent.new("A meticulous inspection of #{name} reveals nothing of interest.",player) 
	end
	
	def to_s
		"#{indefinite} #{name}"
	end
	
end

#this is the base class for all living creatures: Human players, AIs
# it has attributes like: health, race, strength, dexterity, speed
class Creature < Mappable
	attr_reader :title, :race, :health, :armour
	def initialize(name,title = nil,race = nil)
		@title = title ? title : ""
		@race = race ? race : "Human"
		@health = 100
		@armour = 0
		super(name)
	end
end

require 'items.rb'


#return subclasses of Location...
def location_types
	result = []
	ObjectSpace.each_object(Class) do |klass|
    puts klass.method(:new).arity if klass < Location 
		result = result << klass if klass < Location #&& (klass.method(:new).arity == 0)
	end
	result
end

def mappable_types
	result = []
	ObjectSpace.each_object(Class) do |klass|
		result = result << klass if (klass < Mappable) #&& (klass.method(:new).arity == 0)
	end
	result
end
