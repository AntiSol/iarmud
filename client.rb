require 'dbus'
require 'blowfish.rb' 
require 'base64'
require 'thread'

def rcfile; "cliclient.rc" end

class MudClient
	attr_reader :bus
	def initialize
		@initialcommands = []

		message "IARMUD - CLI Client v1.0"
		message "Reading configuration..."
		read_config

		message "Initialising DBus..."
		begin
			@running = true
			@bus = DBus.session_bus
			@service = @bus.service("org.iarmud.service")
			@iarmud = @service.object("/org/iarmud/interface")
			@iarmud.introspect
			@iarmud.default_iface = "org.iarmud.interface"
			@interface = @iarmud["org.iarmud.interface"]

			#handle player events
			@iarmud.on_signal("PlayerEvent") do |player, encryptedText|
				#message "...an event has occurred for #{player}..."
				if player == @name
					crypt = Crypt::Blowfish.new(@token.force_encoding('ASCII'))
					text = crypt.decrypt_string(Base64.decode64(encryptedText))
					message "--> #{text}"
				end 
			end			
		rescue
			message "ERROR: could not connect to DBus!\n"
			throw :dbus_connection_error
		end
		message "DBus initialised."

		if @token then
			puts "Logging in with #{@token}"
		else
			begin 
				if @name == nil then
					puts "Your name?"
					@name = gets.chomp
				end
				@token = @interface.spawn(@name)[0]
				if @token.length != 32 or (@token.count ' ') > 0 then
					# Probably an error message
					puts @token
					@name = @token = nil
				end
			end while @token == nil
		end
		
		message "Your player token is: #{@token}"
		
		message "Type 'exit' to exit the CLI client.\n"
		
		message @interface.playerCommand(@token,'look')[0]
		
	end

	# Read configuration and command-line paramters
	def read_config
		if File.exist? rcfile
			rc = File.new("cliclient.rc")
			while (line = rc.gets)
				line = line.strip
				next if line[0] == '#'	# Comment
				if line[0] == '!' then	# Special command
					line.scan(/!([^ ]+) (.*)/).map do |command, param|
						case command.intern
							when :name; @name = param
							when :token; @token = param
							else; throw "Unknown configuration option: #{line}"
						end
					end
				else
					@initialcommands.push line
				end
			end
			rc.close
			throw "You need to specify both a !name and !token in your .rc file! " if (@token && !@name) or (@name && !@token)
		end

		# Disabled until we fix gets to not try to read file file given in args
		#state = :initial
		#ARGV.each do |arg|
		#	if arg[0] == '-' then
		#		state = (arg.sub /./, '').intern
		#	else
		#		case state
		#			when :initial; @token = arg
		#			when :name; @name = arg
		#			else; throw "Unknown argument #{state}"
		#		end
		#		state = :read
		#	end
		#end
	end
	
	def message(text)
		puts "#{text}\n"
	end
	
	def run
		mutex = Mutex.new
		t = Thread.new do
			#this replaces the Dbus loop...
			while @running do
				mutex.synchronize do
					ready, mert, bork = IO.select([@bus.socket],nil,nil,0.1)
					if ready
						@bus.update_buffer
						while m = @bus.pop_message
							@bus.process(m)
						end
					end
				end
				sleep 0.2
			end
		end

		while @running do
			if @initialcommands.length > 0 then
				cmd = @initialcommands.shift
			else
				cmd = gets.chomp
			end
			@running = false if cmd == "exit" or cmd == "quit" or cmd == "q"
		
			if @running	
				message "\n> #{cmd.upcase}\n" #oldskool... ;)
				mutex.synchronize do
				resp =  @interface.playerCommand(@token,cmd)
				message resp[0] 	#this will be 'OK' or an error, 
									# indicating the command interpreter's response, 
									# *not the game engine's response to the commands*
				end
			end
			message "---\n"
		end

		message "\n\nExiting...\n"
		
	end
end

