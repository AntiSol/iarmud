
#this is a base class for events which occur within the world.
class Event
	attr_accessor :text, :before, :after, :visible_to
	def initialize(text)
		@text = text
	end
	
	def to_s
		@text
	end
	
end

#something which happens to a Player
class PlayerEvent < Event
	def initialize(text,player)
		@player = player
		super(text)
	end
end

#notifies players at the specified coordinates
class NotificationEvent < Event
	def initialize(text,coords,exceptions = [])
		@coords = coords
		@exceptions = exceptions
		super(text)
	end
	def execute
		@coords.each { |coord|
			$game.world.location(coord).players.each { |player|
				player.notify(text) if !@exceptions.find { |e| e == player }
			}
		}
	end
end

class NosePickEvent < Event
	def initialize(player)
		@player = player
		@picktick = 0
		super("#{player} picks his nose")
	end
	def execute
		@picktick+=1
		case @picktick
		when 1
			@player.action = "picking his nose"
			@player.action_expiry = $ticks_elapsed + 80
			Notifier.new(:player => @player, :text => "You start picking your nose",
				:ob_text => "#{@player} begins picking his nose").execute
		when 10
			Notifier.new(:player => @player, 
				:text => "You find a booger. you start drawing it out...",
				:ob_text => "#{@player} is now well into picking his nose").execute
		when 42
			Notifier.new(:player => @player, 
				:text => "You finally get the booger... it's big and green... you stare at it, admiring it's glistening surface.",
				:ob_text => "#{@player} takes his finger out of his nose and stares at it intently").execute
		
		when 50
			@player.notify("...it's mostly solid, with a slight slimy overlay.")
				
		when 80
			Notifier.new(:player => @player, 
							:text => "You eat the booger - YUM!",
							:ob_text => "#{@player} eats booger...").execute		
			#@player.action = nil
			#@player.action_expiry = 0
		end
		$game.queue_event(self) if @picktick <= 80
	end
end

#Notifier Class.
# this is a generic class for notifying players of events.
#	you give it a hash of options on init.
# usage:
# 	n = Notifier.new(
#
#		:player => player,
#			#required. must be a player:
#
#		:text => "You do something"
#			#text sent to player. required.
#
#		:ob_text => "#{player} does something"
#			#observer text. observers are everyone in same location as 
#			# player who is not :affected
#
#		:ob_plus => (Player|Array of players)
#			# players added to observers list.
#
#		:ob_except => (Player|Array)
#			# players excluded from observers list
#
#		:aff_text => "#{Player} does something to you", 
#			#text sent to affected players
#
#		:affected => (Player|Array),
#			#list of affected players
#
#		:extra_text => "you hear something happening nearby",
#			#if this is not specified, :extras will get :ob_text
#
#		:extra => (Player|Coordinate|Array),
#			#list of players / coordinates to get extra_text
#
#		:extra_except => (Player|Array),
#			#exceptions for :extra_text
#
#	)
#
#	n.execute
#
# Examples:
#
# Notifier(:player => dale, :text => "You die").execute 
# [equivalent to dale.notify("You Die")]
#
# Notifier(
#	:player => dale, 
#	:text => "You kill everyone else in the room", 
#	:ob_text => "#{dale} kills you"
# ).execute
#
# A much more likely example:
#
# Notifier(:player => dale, :text => "You kill #{julian}", 
#	:ob_text => "#{dale} kills #{julian}",
#	:aff_text => "#{dale} kills you",
#	:affected => julian,
#	:extra_text => "you hear #{julian}'s death scream nearby",
#	:extra => dale.coord.adjacents
# ).execute
#
# A real-world example: 
#
#Notifier.new(
#	:player => @player, :text => "You go #{@direction}",
#	:ob_text => "#{@player} goes #{@direction}",
#	:extra_text => "#{@player} enters",
#	:extra => newc
#).execute

class Notifier
	def initialize(args)
		err = false
		
		@keys = [
			:player, :text, # player / text he sees
			:ob_text, :ob_plus, :ob_except,	# observer text, extra ob(s), ob exception(s)
			:aff_text, :affected, #affected text, affected player(s)
			:extra_text, :extra, :extra_except #extra text, extra (coord/player)(s), exceptions
			]
			
		@required = [:player, :text]
		
		@keys.each { |key|
			v = args.find{ |k,v| k == key }
			val = v ? v[1] : nil
			#special cases...
			case key
			when :affected, :extra, :extra_except, :ob_plus, :ob_except
				val = [] if !val 
				val = [val] if !val.is_a?(Array) #auto-convert single values to arrays
			when :player
				throw ":player must be a Player!" if !val.is_a?(Player)
			end
			instance_variable_set("@#{key}",val)				
		}

		required.each { |k|
			err = true if !args.find{ |key,v| key == k}
		}
		throw "Notifiers need the following specified: #{required}" if err
		
	end
	
	def method_missing(key,val=nil)
		if val
			key = key.to_s[0,key.to_s.length - 1].to_sym 	# if (key.to_s[-1,1] == "=")
			return instance_variable_set("@#{key}",val) if @keys.find { |k| k == key}
			throw "'#{key}' is not a valid key!"
		end
		instance_variable_defined?("@#{key}") ? instance_variable_get("@#{key}") : nil
	end
	
	def execute
		
		#notify the player
		@player.notify(@text)
		
		#notify observers
		observers = ($game.world.location(@player.coord).players || []) 
		observers += @ob_plus
		observers.each { |p|
			next if (p == @player) or 
				(!p.is_a?(Player)) or 
				((@affected + @ob_except).find { |a| a == p })
				
			p.notify(@ob_text)
		}
		
		#notify affected players.
		@affected.each { |p|
			next if (!p.is_a?(Player))
			p.notify(@aff_text)
		} if @aff_text
		
		#notify extra observers
		@extra_text = @ob_text if !@extra_text
		@extra.each { |x|
			if x.is_a?(Coordinate)
				$game.world.location(x).players.each { |p|
					next if !p.is_a?(Player) or 
						@extra_except.find { |a| a == p }
					p.notify(@extra_text)
				}
			else
				next if !x.is_a?(Player) or 
					@extra_except.find { |a| a == x }
				x.notify(@extra_text)
			end
		}
		
	end
end	

#notify a specific player
class PlayerNotificationEvent < PlayerEvent
	def execute
		@player.notify(text)
	end
end

class PlayerLookEvent < PlayerEvent
	def initialize(player,brief=false)
		@player = player
		@brief = brief
	end
	def execute
		if @brief
			@player.notify($game.world.location(@player.coord).title)
		else
			Notifier.new(
				:player => @player, :text => "You look around...\n\n#{@player.look}",
				:ob_text => "#{@player} looks around"
			).execute
		end
	end
end

class PlayerTunnelEvent < PlayerEvent
	def initialize(player, direction)
		@direction = direction
		super(player.to_s + " tunnels " + direction,player)
	end
	def execute
		if @player.coord.respond_to?("dir_#{@direction}")
			newc = @player.coord.send("dir_#{@direction}") 
			if !newc.is_a? Coordinate
				@player.notify(newc)
				return false
			end
		else 
			@player.notify("that's not a direction!")
			return false
		end
				
		newloc = $game.world.location(newc)

		if !newloc.is_a?(Rock)
			@player.notify("You can't tunnel into an existing location!")
			return false
		end
		
		tunnel = Tunnel.new
		$game.world.add_location(newc,tunnel)
		
		Notifier.new(:player => @player, :text => "You tunnel #{@direction}",
			:ob_text => "#{@player} tunnels #{@direction}").execute
			
		#move the player...
		$game.queue_event(PlayerMovementEvent.new(@player,@direction))

	end
end

class PlayerMovementEvent < PlayerEvent
	def initialize(player, direction)
		@direction = direction
		super(player.to_s + " goes " + direction,player)
	end
	def execute
		if @player.coord.respond_to?("dir_#{@direction}")
			newc = @player.coord.send("dir_#{@direction}") 
			if !newc.is_a? Coordinate
				@player.notify(newc)
				return false
			end
		else 
			@player.notify("that's not a direction!")
			return false
		end
				
		newloc = $game.world.location(newc)
		
		if newloc.is_a? Rock	#movement fail, others laugh...
			Notifier.new(
				:player => @player,
				:text => "You try to go #{@direction}, but you run into #{newloc.title}!",
				:ob_text => "Suddenly, #{@player.name} bursts into action: " +
				"he runs #{@direction} at full speed, slamming face-first into " +
				"#{newloc.title}. He bounces, lands on his ass, and stands up again " +
				"somewhat shakily. You find this rather amusing."
			).execute
			return false
		end
		
		#movement will succeed...
		
		#do notifications...
		Notifier.new(
			:player => @player, :text => "You go #{@direction}",
			:ob_text => "#{@player} goes #{@direction}",
			:extra_text => "#{@player} enters",
			:extra => newc
		).execute

		#move the player...
		$game.world.move_mappable(@player,newc)
		#update coords for inventory items...
		@player.inventory.each { |i| i.coord = newc } 
		
		#move automatically looks...
		PlayerLookEvent.new(@player,true).execute
		
		return true

	end
end

#something which happens more than once
#	this is the fine-grained version - you
#	probably want TimedRepeatingEvent 
class RepeatingEvent < Event
	def initialize(text,times = 2)
		@repeats = times
		super(text)
	end
end

#something which happens more than once,
#	at a timed interval (in secs) 
# note that timing cannot be precise,
#	and will become less accurate as 
#	interval approaches tick granularity
class TimedRepeatingEvent < RepeatingEvent
	def initialize(text, times = 2, interval = 1)
		@interval = interval
		super(text,times)
	end
end

#something which happens after an interval.
#	Fine-grained version - see TimedDelayedEvent
class DelayedEvent < Event
	def initialize(text,ticks = 1)
		@run_at = ticks + $ticks_elapsed
		super(text)
	end
end

#something which happens after a number of seconds
class TimedDelayedEvent < Event
	def initialize(text, interval = 1)
		@run_at = Time.new + interval
		super(text)
	end
end

class FailureEvent < Event
	def to_s
		"EPIC FAIL: #{@text}"
	end
end

