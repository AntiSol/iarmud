# core classes for iarMUD
# built on ruby 1.9.1

require 'dbus'
require 'digest/md5'
require 'world.rb'
require 'items.rb'
require 'interpreter.rb'
require 'player.rb'
require 'events.rb'

$DEBUG = true
#output a server message every tick (this is extreme)
$SHOW_TICKS = false
#seconds between game ticks. not necessarily precise - this is 
#	actually a minimum length - a tick might have alot going on,
#	and may take more than this long to execute.
$TICK_GRANULARITY = 0.2

#how often will 'alive' messages be shown on the server (seconds)
#	(these are the messages that show timing variance)
$ALIVE_INTERVAL = 30

#number of ticks since game start
$ticks_elapsed = 0


#outputs a server message to the screen
def message(str)
	puts "#{Time.now.strftime("%H:%M:%S.%L")}\t##{sprintf("%07d",$ticks_elapsed)}:\t#{str}"
end

def debug(str)
	message str if $DEBUG 
end

#this is the DBus interface we export
class MUD_DBus < DBus::Object
	dbus_interface "org.iarmud.interface" do
		#create a new player
		dbus_method :spawn, "in name:s, out key:s" do |name|
			a = $game.addplayer(p = Player.new(name))
			if (a == true)
				message "DBUS: a player named '#{name}' spawns!"
				return [p.key]
			else
				return a
			end
		end
		
		#player executes a command
		dbus_method :playerCommand, "in playerKey:s, in command:s, out ret:s" do |key, command|
			player = $game.getplayer(key)
			return ["'#{key}' is not a valid playerKey"] if !player
			message "DBUS: Player '#{player}' issues command: '#{command}'" if $DEBUG
			#["#{$game.interpreter.interpret(player,command)}"]
			player.command_queue.push(command)
		end
		
		#print the god token on the server log
		dbus_method :godToken, "out ret:s" do
			message "God Token is: '#{@god_token}'"
			"God Token output to server log"
		end
		
		#you don't need to be a player to get a map...
		dbus_method :map, "in x:i, in y:i, in z:i, in width:i, in height:i, out ret:s" do |x,y,z,w,h|
			message "Map requested (#{x},#{y},#{z}), #{w}x#{h}"
			$game.world.render_ascii_size(Coordinate(x,y,z),w,h)
		end
  
		dbus_signal :PlayerEvent, "player:s, encrypted_text:s"
		
	end
end

#this is our main game engine.
class MUD
	
	attr_reader :world, :bus, :players, :interpreter, :god_token, :dbus_interface
	
	def initialize
		
		@VERSION_MAJOR = 0
		@VERSION_MINOR = 1
		@players = []
		@abbrevs = [
			"Interface Agnostic Ruby MUD",
			"Intensely Awesome, Really! MUD",
			"Intrinsically Average, Really, MUD",
			"Informatively Artistic, Realistic MUD",
			"Intelligently Artificial Realtime MUD",
			"Intellectually Ambiguous Relativistic MUD",
			#add more!
		]
		
		#setup the server...
		
		$game = self
		
		message title
		message "Server Init"
		
		@god_token = Digest::MD5.hexdigest("GOD_#{rand(999999999)}")
		message "- God token for this instance: '#{@god_token}'"				
		
		message "- #{$ALIVE_INTERVAL} seconds will be about #{$TICK_MOD = ($ALIVE_INTERVAL / $TICK_GRANULARITY).to_i} ticks."
		@last_tick_time = Time.now
		
		#setup the world...
		
		@world = World.new

		#load saved world...
		load('iarmud.map.gz') if File.exists? 'iarmud.map.gz'
		
		@event_queue = []
			
		#are we processing the queue ATM?
		@in_queue = false
			
		#queue for next tick...	
		@next_queue = [] 	
		
		@interpreter = Interpreter.new
		
		
		#setup DBus...
		message "Registering server with DBus..."
		
		@bus = DBus.session_bus
		@service = @bus.request_service("org.iarmud.service")
		
		@dbus_interface = MUD_DBus.new("/org/iarmud/interface")
		@service.export(@dbus_interface)
		
		message "DBus initialized."
		
	end	

	#add an event (or array of events) to the event queue.
	#	the event queue will be processed next tick.
	#	if this is called from within the event queue (i.e from an event's 
	#		'execute' method), the event will occur *next tick*, not at the end
	#		of the current one
	def queue_event(event)		
		#handle arrays...
		if event.is_a?(Array)
			event.each do |e|
				queue_event(e)
			end
			#message("Queued #{event.length()} events.")
		else		
			#Note: this is a bit of a kludge, we should probably
			#	not auto-convert strings to events, because
			#	we don't know who they happen to, or where they happen
			event = Event.new(event) if !event.is_a?(Event)
			
			if @in_queue	#add it to the next tick...
				return @next_queue.push(event)
			end
			@event_queue.push(event)
		end
	end	
		
	def version
		"#{@VERSION_MAJOR}.#{@VERSION_MINOR}"
	end
	
	def title
		#return @@title if @@title
		@title = "IARMUD - #{@abbrevs.shuffle[0]}, v#{version}"
	end
	
	def getplayer(key)
		@players.find { |p| p.key == key }
	end
	
	def playerbyname(name)
		@players.find { |p| p.name == name }
	end
	
	def addplayer(player)
		return false if getplayer(player.key)
		if playerbyname(player.name) != nil
			message "Spawn error - a player named '#{player.name}' already exists"
			return "There's already a player named '#{player.name}'!" 
		end
		@players.push(player)
		return true
	end
	
	def load(filename)
		message "Loading World State..."
		@world = ObjectStash.load filename
		
		#players are saved with the world, so we find saved players and add them to the game...
		@players += @world.find_players
		
		message "World State Loaded."
		
		return self
	end
	
	def save(filename)
		message "Saving World State..."
		ObjectStash.store @world, filename 
		#, :gzip => false
		message "World Saved."
	end
	
	#function for autonomous events - called once per tick.
	def tick
		
		#TODO: put AI / "world-event" code here
		
		#process player command queues...
		@players.each { |player|
			player.action = nil if $ticks_elapsed >= player.action_expiry
			next if player.action
			cmd = player.command_queue.shift if player.command_queue.length > 0
			@interpreter.interpret(player,cmd)
		}
		
		#process the event queue...
		@in_queue = true
		while @event_queue.length > 0 do
			evt = @event_queue.shift
			if evt.respond_to?(:execute)
				evt.execute
			end
		end
		@in_queue = false			
		@event_queue = @next_queue
		@next_queue = []
	end
	
	#this tick runs roughly once per second, assuming tick granularity <= 1
	#	lowering tick granularity (and keeping it a factor of 1) 
	#	should improve precision
	def second_tick
		
	end
	
	#starts the game
	def run
		message "Game Starting!"
		start_time = Time.now
		#the main loop...
		while @world != nil
			$ticks_elapsed+=1
			message "---Tick!---" if $SHOW_TICKS
			
			if ($ticks_elapsed % $TICK_MOD.to_i == 0)
				#calculate timing variance...
				now = Time.now
				precision = ((now - start_time) / ((start_time + ($ticks_elapsed * $ALIVE_INTERVAL)) - now)) * 100
				#show alive message...
				message "Game Time: #{$ticks_elapsed} Ticks (Timing variance: #{ '%.3f' % precision}%)" 
			end
			starttime = Time.now.strftime("%s.%L").to_f
			#this replaces the Dbus loop...
			ready, mert, bork = IO.select([@bus.socket],nil,nil,$TICK_GRANULARITY)
			if ready
				@bus.update_buffer
				while m = @bus.pop_message
					@bus.process(m)
				end
			end
			
			tick
			
			if Time.now - @last_tick_time >= 1
				message "---Second Tick!---" if $SHOW_TICKS
				second_tick
				@last_tick_time = Time.now
			end
			
			#compensate for dbus events coming in faster than our tick granularity,
			#	otherwise ticks can take less time to elapse, which introduces timing issues
			waittime = Time.now.strftime("%s.%L").to_f - starttime
			compensation = ($TICK_GRANULARITY - waittime).to_f
			if compensation > 0
				sleep compensation
			end
			
		end
	end
		
	end
	
	
	require 'zlib'

# Save any ruby object to disk!
# Objects are stored as gzipped marshal dumps.
# "borrowed" from: http://marcuswestinblog.blogspot.com/2008/03/save-ruby-objects-to-disk-for-later.html
#
# Example
#
# # First ruby process
# hash = {1 => "Entry1", 2 => "Entry2"}
# ObjectStash.sotre hash, 'hash.stash'
#
# ...
#
# # Another ruby process - same_hash will be identical to the original hash
# same_hash = ObjectStash.load 'hash.stash'
class ObjectStash

  # Store an object as a gzipped file to disk
  # 
  # Example
  #
  # hash = {1 => "Entry1", 2 => "Entry2"}
  # ObjectStore.store hash, 'hash.stash.gz'
  # ObjectStore.store hash, 'hash.stash', :gzip => false
  def self.store obj, file_name, options={}
    marshal_dump = Marshal.dump(obj)
    file = File.new(file_name,'w')
    file = Zlib::GzipWriter.new(file) unless options[:gzip] == false
    file.write marshal_dump
    file.close
    return obj
  end
  
  # Read a marshal dump from file and load it as an object
  #
  # Example
  #
  # hash = ObjectStore.get 'hash.dump.gz'
  # hash_no_gzip = ObjectStore.get 'hash.dump.gz'
  def self.load file_name
    begin
      file = Zlib::GzipReader.open(file_name)
    rescue Zlib::GzipFile::Error
      file = File.open(file_name, 'r')
    ensure
      obj = Marshal.load file.read
      file.close
      return obj
    end
  end
end
