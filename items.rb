# Prototype Item definitions which should easily allow us to set
# additional attributes and customize how an Item works in the class
# definition.

# Add some helpful definitions
# Unfortunately, these can only overwrite previous definitions,
# not add to them (eg, add more components to an item)
# To do that, add code to the initialize method to do so.
class Class
	# Shorthand for defining an attribute. You can then check if an Item has
	# a particular attribute using the .has? function
	def attribute! accessor, value
		define_method(accessor) do
			name = "@#{accessor}"

			instance_variable_set(name, value) unless instance_variable_defined?(name)
			instance_variable_get(name)
		end

		define_method("#{accessor}=") do |val|
			instance_variable_set("@#{accessor}", val)
		end

		define_method("has_#{accessor}") do
			true
		end
	end

	# Similar to the above, but just for a true/false on "IS this?"
	def define_types! types, prefix = "is"
		types.each do |type|
			define_method("#{prefix}_#{type}") do
				true
			end
		end
	end

	# You can use .is? :wieldable for example.
	# You can use multiple definitions, eg: type! :weapon, :wieldable
	def type! *types
		define_types! types, "is"
	end

	# Shorthand for defining the components for an item.
	# Components go into the forging, and ideally will end up with bonuses for
	# each component, depending on what raw material is used
	def components! *components
		attribute! :components, components
		components.each do |c|
			attribute! "component_#{c}", nil
		end
	end

	# Define a setlist, for example engravings (see below)
	def int_define_setlist! what, single, multi
		what.each do |wh|
			define_method("#{single}_#{wh}") do; true end
			define_method("#{multi}_#{wh}") do; instance_variable_get("@#{multi}_#{wh}") rescue nil end
			define_method("#{multi}_#{wh}=") do |val|; instance_variable_set("@#{multi}_#{wh}", val) end
		end
	end

	# Allow components given to be engraved
	# Eg, item.engravable? :blade
	#  -> item.engraving_blade = "Foo"
	def engravable! *which
		int_define_setlist! which, "engravable", "engraving"
	end

	# Allow components to be socketable
	def socketable! *which
		int_define_setlist! which, "socketable", "socket"
	end
end

#this is something which goes in the world which is not a player.
#	it has things like: weight (can a player pick it up? should it just be 'carryable'?)
class Item < Mappable
	def has? what; respond_to? "has_#{what}" end
	def is? what; respond_to? "is_#{what}" end
	def component what, is; send "component_#{what}=", is end
	def engravable? what; respond_to? "engravable_#{what}" end
	def engraving what, with; send "engraving_#{what}=", with end
	def socketable? what; respond_to? "socketable_#{what}" end
	def socket what, with; send "socket_#{what}=", with end

	# Override if you like
	def name; "#{self.class}" end

	def description
		return @name if !self.respond_to?(:components)

		details = [] 
		self.components.each do |component|
			c = send("component_#{component}")
			d = "the #{component} is "
			if c
				d += c
				if socketable? component
					socket = send("socket_#{component}")
					d += socket ? " socketed with #{socket}" : " (socketable)"
				end
				if engravable? component
					engraving = send("engraving_#{component}")
					d += engraving ? " engraved with '#{engraving}'" : " (engravable)" 
				end
			else
				d += "missing"
			end
			details.push d
		end

		"#{indefinite} #{self.name}; " + details.take(details.size - 1).join(", ") + " and " + details.last + "."
	end
	
	# Default attributes
	attribute! :weight, 1

end

# Define a weapon and some base information
class Weapon < Item
	type! :weapon, :wieldable
	attribute! :type, "notset"
	attribute! :indefinite,"a"
end

# Extend our definition, and add some more details
class BladedWeapon < Weapon
	attribute! :type, "bladed"
	components! :blade
end

class Knife < BladedWeapon
	# TODO: Find out how to inherit components from parents
	# May require using the initializer instead
	type! :one_handed
	components! :blade, :handle
	engravable! :blade
end

class Claymore < BladedWeapon
	attribute! :weight, 4
	type! :two_handed
	components! :handle, :pommel, :handguard, :blade
	engravable! :blade
	socketable! :pommel
end


