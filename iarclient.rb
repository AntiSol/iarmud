#iarclient.rb
#an IARMUD client class for general use in many clients

require 'dbus'
require 'blowfish.rb' 
require 'base64'

#The IARMUD Client Class
#	this is intended to make it very easy to create an iarmud client
#	it connects to DBus automagically, and provides easy ways to:
#		- receive signals
#		- send commands
#		- manage player keys / decryption
#		- will provide multiple 'run' methods:
#			- a standard DBus loop. this might do for a TCP server.
#			- a threaded DBus loop, allowing asynchronous commands / signals
#		- it will probably provide a full CLI client
#	Usage should be *really* simple!
#
# Notes: 
# * the IARClient object provides .stdin, .stdout, and .stderr params - note that gets and puts have been overridden
#		within the object
#
class IARClient
	attr_accessor :bus, :interface, :tick_granularity
	attr_reader :ticks_elapsed, :running
	
	#parse a hash of arguments and set IARClient settings accordingly.
	#	called during initialize
	def parse_args(args = {})
		args.each_pair { |key,val|
			case key
			when :stdout
				stdout(val)
			when :stdin
				stdin(val)
			when :stderr
				stderr(val)
			else
				erromsg "Invalid IARClient argument: #{key}"
				throw :invalid_argument_error
			end
		}
	end
	
	### IO Stuff ###
	#	stdin / out / err can be provided to the IARClient to have
	#	it write to a file ( or tcp socket or whatever [?] )
	
	#	we probably want to make this smarter: only open/close the IO
	#	during gets / puts. but We need to know the diff between an IO 
	#	and a file. and I can't be bothered reading the dox right now.
	
	#setters/getters for stdin/out/err
	def stdin(val = nil)
		if val
			val = File.open(val,'r') if val.is_a?(String)
			@stdin = val
		end
		return @stdin
	end
	
	def stdout(val = nil)
		if val
			val = File.open(val,'a') if val.is_a?(String)
			@stdout = val
		end
		return @stdout
	end
	
	def stderr(val = nil)
		if val
			val = File.open(val,'a') if val.is_a?(String)
			@stderr = val
		end
		return @stderr
	end
	
	#overrides for IO functions
	def gets
		@stdin.gets
	end
	
	def puts(val)
		@stdout.puts(val)
	end
	
	def errmsg(val)
		@stderr.puts(val)
	end
	
	def message(text)
		puts "#{text}\n"
	end
	
	### End IO Stuff ###
	
	def initialize(args = {})
		
		#variable init.
		@threaded = false
		
		@stdin = $stdin
		@stdout = $stdout
		@stderr = $stderr
		
		@tick_granularity = 0.2
		@ticks_elapsed = 0
		@running = false
		
		@players = []
		
		#it's important that you don't init instance variables
		#	which can be changed after this
		parse_args(args) if args.length > 0
		
		@mutex = mutex = Mutex.new if @threaded
		
		#connect to DBus...
		message "Initialising DBus..."
		begin
			
			@bus = DBus.session_bus
			@service = @bus.service("org.iarmud.service")
			@iarmud = @service.object("/org/iarmud/interface")
			@iarmud.introspect
			@iarmud.default_iface = "org.iarmud.interface"
			@interface = @iarmud["org.iarmud.interface"]

			#handle player events
			@iarmud.on_signal("PlayerEvent") do |player, encryptedText|
				#message "...an event has occurred for #{player}..."
				#raise signal...
				@on_evt.call(player,encryptedText) if @on_evt
				
			end			
		rescue
			errmsg "ERROR: could not connect to DBus!\n"
			throw :dbus_connection_error
		end
		message "DBus initialised."
	end
	
	#decrypt text with the given key (player's token)
	def decrypt(key,encryptedText)
		crypt = Crypt::Blowfish.new(key.force_encoding('ASCII'))
		crypt.decrypt_string(Base64.decode64(encryptedText))
	end
	
	#use this to set up an 'event'. your block will be called with 
	#	player and encryptedText as arguments.
	def on_playerEvent(&block)
		@on_evt = block
	end
	
	#this event is called every tick
	#	block is called with no arguments.
	def on_tick(&block)
		@on_tick = block
	end
	
	def playerCommand(key,command)
		@interface.playerCommand(key,command)[0]
	end
	
	def spawn(name)
		@interface.spawn(@name)[0]
	end
	
	def run
		###the standard, no-frills DBus loop:###
		#loop = DBus::Main.new
		#loop << @bus
		#loop.run
		######
		
		@running = true
		
		#t = Thread.new do
			#this replaces the Dbus loop...
			while @running do
				#mutex.synchronize do
					ready, mert, bork = IO.select([@bus.socket],nil,nil,0.1)
					if ready
						@bus.update_buffer
						while m = @bus.pop_message
							@bus.process(m)
						end
					end
					@on_tick.call if @on_tick
				#end
				sleep @tick_granularity
			end
		#end
		
	end

end

#Playing around. one day this will be commented out with a 'sample usage:' message
#puts "Your Name?"
#name = gets.chomp
#
#puts "Your key?"
#key = gets.chomp
#
#client = IARClient.new()
#
#client.on_playerEvent do |player,text|
#	#puts "event for: #{player}: #{text}"
#	#puts "Decrypted:"
#	puts client.decrypt(key,text) if player == name
#end
#
#client.on_tick do 
#	#puts "Tick!"
#end
#
#client.run