require 'blowfish.rb' 
require 'base64'

#human player
class Player < Creature
	attr_reader :key, :coord, :inventory, :is_god
	attr_accessor :name, :title, :command_queue, :action, :action_expiry
	def initialize(name)
		@name = name
		@key = Digest::MD5.hexdigest("#{name}_#{rand(999999999)}")
		super(name)
		@coord = Coordinate(nil,nil,nil)
		@inventory = Array.new
		@title = ""
		@is_god = false
		
		@command_queue = []
		
		@action = nil
		@action_expiry = 0
		
		#find a spawning pool to spawn in...
		pool = $game.world.find_locations(nil,SpawningPool).shuffle[0]
		#put the player in the world on create... 
		$game.world.add_mappable(pool.coord,self)
	end
	
	#shortcuts for coordinate
	def x
		@coord.x
	end
	def x=(x)
		@coord.x = x
	end
	def y
		@coord.y
	end
	def y=(y)
		@coord.y = y
	end
	def z
		@coord.z
	end
	def z=(z)
		@coord.z = z
	end
	
	def add_to_inventory(mappable)
		if mappable.coord.in_world
			#remove from room...
			$game.world.remove_mappable(mappable)
		end
		if inventory.find { |i| i == mappable }
			return PlayerNotificationEvent.new("You're already carrying #{mappable}!",self)
		end
		#add to inventory
		mappable.coord = coord
		@inventory.push(mappable)
		PlayerNotificationEvent.new("You pick up #{mappable}",self)
	end
	
	def remove_from_inventory(mappable)
		@inventory.delete(mappable)
	end
	
	def pickup(mappable)
		add_to_inventory(mappable)
	end
	
	def god(god_key)
		if god_key == $game.god_token
			@is_god = true
			"Your divine influence radiates over the entire world"
		else
			"Nah."
		end
	end
	
	def look
		loc = $game.world.location(@coord)
		if !loc
			return "You're in a very strange place..."
		end
		ret = "#{loc.coord}: #{loc.title}\n\n#{loc.description}"
		exits = loc.exits
		ret += "\n\n#{exits}" if exits != ""
		contents = loc.contents_as_s(self.name)
		ret += "\n#{contents}" if contents != ""
		ret
	end

	def tunnel(direction)
		if coord.respond_to?("dir_#{direction}")
			newc = coord.send("dir_#{direction}") 
			return newc if !newc.is_a? Coordinate
		else 
			return "that's not a direction!"
		end
		room = $game.world.location(newc)
		return "You can't tunnel into an existing room!" if !room.is_a? Rock
		
		newloc = Tunnel.new
		$game.world.add_location(newc,newloc)
		return "You tunnel #{direction}.\n#{self.move(direction)}"
	end
	
	def notify(text)
		if $DEBUG
			txt = text.gsub("\n","<LF>")
			txt = txt[0..30] + '...' if txt.length > 32
			message "Notify #{self}: '#{txt}'"
		end
		#encrypt text using @key, then base64 encode that so that DBus
		#	doesn't have a heart-attack transmitting binary data
		crypt = Crypt::Blowfish.new(@key.force_encoding('ASCII'))
		e = Base64.encode64(crypt.encrypt_string(text)) 
		
		#send teh event!
		$game.dbus_interface.PlayerEvent(@name,e)
	end
	
	def to_s
		ret = @name
		ret += " #{@title}" if @title.length > 0
		ret
	end
end